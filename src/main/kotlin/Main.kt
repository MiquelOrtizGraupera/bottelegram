import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.command
import com.github.kotlintelegrambot.dispatcher.text
import com.github.kotlintelegrambot.entities.ChatId
import java.time.LocalDateTime

fun main() {
    val mutableMap: MutableMap<Long, String> = mutableMapOf()
    val bot = bot {
        token = "5610409121:AAE0u6lXHCgkSlAwQnifLs62IYXcm_9sIk8"
        dispatch {
            command("start"){
                val result = bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = "Que passa tiu!")
                result.fold({
                    bot.sendMessage(ChatId.fromId(message.chat.id), text = "Que vols fer?")
                },{
                    bot.sendMessage(ChatId.fromId(message.chat.id), text = "Tenim un error")
                })
            }
            text("hola") {
                bot.sendMessage(ChatId.fromId(message.chat.id), text = "Adéu")
            }
            command("today"){
                val current = LocalDateTime.now()
                bot.sendMessage(ChatId.fromId(message.chat.id), text = current.toString())
            }
            command("store"){
                val toStore = args.joinToString()
                bot.sendMessage(ChatId.fromId(message.chat.id),text = "Info Saved!")
                mutableMap[message.chat.id] = toStore
            }
            command("retrieve"){
                for (key in mutableMap.keys){
                    if(message.chat.id == key){
                        println(mutableMap.values)
                        println(mutableMap.keys)
                        bot.sendMessage(ChatId.fromId(message.chat.id), text = mutableMap.values.toString())
                    }
                }
            }
        }
    }
    bot.startPolling()
}